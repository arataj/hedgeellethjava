/*
 * JavaFrontend.java
 *
 * Created on Apr 14, 2009, 2:04:29 PM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.frontend.java;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.dereference.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.flags.MethodFlags;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BlockExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.frontend.java.parser.JavaParser;
import pl.gliwice.iitis.hedgeelleth.interpreter.*;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.modules.AbstractFrontend;

/**
 * A frontend of a subset of Java, supported by Java2TADD.<br>
 *
 * For frontend compatibility, as the semantic checker it returns
 * <code>SemanticCheck</code> instead of <code>JavaSemanticCheck</code>,
 * and implements the functionality missing in <code>SemanticCheck</code>
 * in the method <code>semanticCheck()</code>.
 *
 * @author Artur Rataj
 */
public class JavaFrontend extends AbstractFrontend {
    /**
     * Default file name extension.
     */
    public static final String FILE_NAME_EXTENSION = "java";
    /**
     * Creates a new instance of JavaFrontend.
     */
    public JavaFrontend() {
        super();
    }
    @Override
    public void parse(Compilation compilation, PWD topDirectory,
            String libraryPath, String fileName, CompilerException parseErrors) {
        // System.out.print("Parsing " + fileName + "...");
        // System.out.flush();
        JavaParser parser = null;
        boolean ioError = false;
        try {
                parser = new JavaParser(compilation, this, topDirectory,
                        libraryPath, fileName);
        } catch(IOException e) {
            parseErrors.addAllReports(new CompilerException(
                    null, CompilerException.Code.IO,
                    "could not compile file `" + fileName + "': " +
                    e.getMessage()));
            ioError = true;
        }
        if(!ioError)
            try {
                try {
                    parser.CompilationUnit(isLibrary(libraryPath, fileName));
                } catch(TokenMgrError f) {
                    if(f.pos != null && f.eofSeen)
                        throw new ParseException(f.pos, ParseException.Code.PARSE,
                            "unclosed comment");
                    else
                        throw f;
                }
            } catch(ParseException e) {
                e.completeStreamName(fileName);
                parseErrors.addAllReports(e);
            }
    }
    @Override
    public Class getSemanticCheckClass() {
        return SemanticCheck.class;
    }
    @Override
    public void semanticCheck(Compilation compilation, SemanticCheck sc,
            BlockExpression blockExpression, CompilerException compileErrors)
            throws CompilerException {
        if(blockExpression instanceof JavaConditionalExpression) {
            JavaConditionalExpression c = (JavaConditionalExpression)
                    blockExpression;
            JavaSemanticCheck.parse(sc, c);
        }
    }
    @Override
    public MethodSignature getMainMethodSignature(String arrayClassName,
            String stringClassName) {
        List<Type> arg = new LinkedList<>();
        Type type = Type.newString(stringClassName);
        type.increaseDimension(arrayClassName);
        arg.add(type);
        MethodSignature signature = new MethodSignature(null,
                "main", arg);
        return signature;
    }
    @Override
    public void setMainMethodArgs(List<String> args, AbstractInterpreter interpreter,
            RuntimeThread thread) throws InterpreterException {
        // using the interpreter programmatically
        // for execution of custom operations
        // it kind--of simulates the normal translation
        // process and is not a normal way of using
        // the interpreter but an exception, just for
        // setting the main method's argument
        Type arrayType = Type.newString(null);
        arrayType.increaseDimension(null);
        // a fake method in a fake class
        Method method = new Method("fake");
        method.flags = new MethodFlags();
        method.flags.context =  Context.STATIC;
        method.refreshSignature();
        method.scope = new MethodScope(method, null,
                method.signature.toString());
        method.scope.blockScope = new BlockScope(method,
                method.scope, "fake");
        method.arg = new LinkedList<>();
        method.annotations = new LinkedList<>();
        CodeClass codeClass = new CodeClass("fake", null);
        codeClass.frontend = this;
        CodeMethod codeMethod = new CodeMethod(codeClass, method);
        RuntimeMethod rm = new RuntimeMethod(codeMethod);
        InterpretingContext context = new InterpretingContext(interpreter,
                thread, true, rm, InterpretingContext.UseVariables.ALL);
        List<RuntimeValue> sizes = new LinkedList<>();
        sizes.add(new RuntimeValue(args.size()));
        RuntimeValue arrayRef = interpreter.newArrays(context, arrayType, sizes,
                "#main_method_args");
        CodeVariable argsCV = new CodeVariable(null, codeMethod, "args",
                arrayType, Context.NON_STATIC, false, false, false,
                Variable.Category.OTHER);
        rm.setValue(interpreter, rm, argsCV, arrayRef);
        for(int index = 0; index < args.size(); ++index) {
            String s = args.get(index);
            if(!s.isEmpty())
                throw new RuntimeException("string objects not implemented");
            RuntimeValue stringRef = interpreter.newRuntimeObject(
                    interpreter.stringClass, interpreter.stringClass.type);
            // !!! no value is written to the string
            CodeIndexDereference d = new CodeIndexDereference(argsCV,
                    new RangedCodeValue(
                        new CodeConstant(null, new Literal(index))));
            interpreter.setValue(d, stringRef, rm);
        }
        thread.startMethodArgs.clear();
        thread.startMethodArgs.add(arrayRef);
    }
}
